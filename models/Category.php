<?php

class category {
    // DB Stuff
    private $connection;
    private $table = 'categories';

    // Properties

    /*
     * @var int
     */
    public $id;
    public $name;
    public $createdAt;

    public function __construct(PDO $db) {
        $this->connection = $db;
    }

    // Get Categories
    public function  read() {
        $query = '
        SELECT id, name, created_at
            FROM
              '. $this->table .'
            ORDER BY
              created_at DESC
        ';

        // Prepare Statement
        $stmt = $this->connection->prepare($query);

        // Execute query
        $stmt->execute();

        return $stmt;
    }

    // Get single Category
    public function readSingle() {
        $query = '
        SELECT id, name, created_at
            FROM '. $this->table .'
            WHERE id = :id
        ';

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':id',$this->id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->createdAt = $row['created_at'];
    }

    public function  create() {
        $query = '
            INSERT INTO '. $this->table .'
              (name, created_at)
            VALUES
              (:name, :createdAt) 
        ';

        $stmt = $this->connection->prepare($query);
        $this->name = htmlspecialchars(strip_tags($this->name));
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':createdAt', $this->createdAt);

        if ($stmt->execute()) {
            return true;
        }

        printf('Error: %s./n', $stmt->errorInfo());
        return false;
    }

    public function update() {
        $query = '
            UPDATE '. $this->table .'
            SET
              name = :name
            WHERE id = :id
        ';

        $stmt = $this->connection->prepare($query);
        $this->name = htmlspecialchars(strip_tags($this->name));
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':id', $this->id);

        // Execute Query
        if ($stmt->execute()) {
            return true;
        }

        // Print Error
        printf('Error: %s.\n', $stmt->errorInfo());

        return false;
    }

    public function delete() {
        $query = 'DELETE FROM '. $this->table .' WHERE id = :id';

        $stmt = $this->connection->prepare($query);
        $this->id = htmlspecialchars((strip_tags($this->id)));
        $stmt->bindParam(':id', $this->id);

        // Execute Query
        if ($stmt->execute()) {
            return true;
        }

        // Print Error
        printf('Error: %s.\n', $stmt->errorInfo());

        return false;
    }
}