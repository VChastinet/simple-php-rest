<?php
class Post {
    //BD Access

    /**
     * @var PDO
     */
    private $connection;

    /**
     * @var String
     */
    private $table = 'posts';

    //Post Properties

    /**
     * @var Int
     */
    public $id;

    /**
     * @var Int
     */
    public $categoryId;

    /**
     * @var String
     */
    public $categoryName;

    /**
     * @var String
     */
    public $title;

    /**
     * @var String
     */
    public $body;
    /**
     * @var String
     */
    public $author;

    /**
     * @var Datetime
     */
    public $createdAt;

    //Constructor with DB
    public function __construct(PDO $db) {
        $this->connection = $db;
    }

    // Get Posts
    public function read() {
        //Create Query
        $query = '
            SELECT c.name AS category_name,
                p.id,
                p.category_id,
                p.title,
                p.body,
                p.author,
                p.created_at
            FROM
                ' . $this->table . ' AS p
            LEFT JOIN
                categories AS c ON p.category_id = c.id
            ORDER BY 
                p.created_at DESC
        ';

        // Prepare Statement
        $stmt = $this->connection->prepare($query);

        //Execute Query
        $stmt->execute();

        return $stmt;
    }

    /**
     * @return String
     */
    public function readSingle() {
        //Create Query
        $query = '
            SELECT c.name AS category_name,
                p.id,
                p.category_id,
                p.title,
                p.body,
                p.author,
                p.created_at
            FROM
                ' . $this->table . ' AS p
            LEFT JOIN
                categories AS c ON p.category_id = c.id
            WHERE p.id = ?
            LIMIT 1
        ';

        //  Prepare Statement
        $stmt = $this->connection->prepare($query);


        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Execute Query
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // Set Properties
        $this->title = $row['title'];
        $this->body = $row['body'];
        $this->author = $row['author'];
        $this->categoryId = $row['category_id'];
        $this->categoryName = $row['category_name'];
    }

    // Create Post
    public function create() {
        // Create Query
        $query = '
            INSERT INTO ' . $this->table . '
            (title, body, author, category_id)
            VALUES (:title, :body, :author, :categoryId)
        ';

        // Prepare Statemen
        $stmt = $this->connection->prepare($query);

        // Clean Data
        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->body = htmlspecialchars(strip_tags($this->body));
        $this->author = htmlspecialchars(strip_tags($this->author));
        $this->categoryId = htmlspecialchars(strip_tags($this->categoryId));

        // Bind data
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':body', $this->body);
        $stmt->bindParam(':author', $this->author);
        $stmt->bindParam(':categoryId', $this->categoryId);

        // Execute Query
        if ($stmt->execute()) {
            return true;
        }

        // Print Error
        printf('Error: %s.\n', $stmt->errorInfo());

        return false;
    }

    // Update Post
    public function update() {
        // Create Query
        $query = '
            UPDATE ' . $this->table . '
            SET
                title = :title,
                body = :body,
                author = :author,
                category_id = :categoryId
            WHERE 
                id = :id
        ';

        // Prepare Statemen
        $stmt = $this->connection->prepare($query);

        // Clean Data
        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->body = htmlspecialchars(strip_tags($this->body));
        $this->author = htmlspecialchars(strip_tags($this->author));
        $this->categoryId = htmlspecialchars(strip_tags($this->categoryId));
        $this->id = htmlspecialchars(strip_tags($this->id));

        // Bind data
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':body', $this->body);
        $stmt->bindParam(':author', $this->author);
        $stmt->bindParam(':categoryId', $this->categoryId);
        $stmt->bindParam(':id', $this->id);

        // Execute Query
        if ($stmt->execute()) {
            return true;
        }

        // Print Error
        printf('Error: %s.\n', $stmt->errorInfo());

        return false;
    }

    public function delete() {
        // Create Query
        $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

        // Clean Data
        $this->id = htmlspecialchars(strip_tags($this->id));

        // Prepare Statemen
        $stmt = $this->connection->prepare($query);

        // Bind data
        $stmt->bindParam(':id', $this->id);

        // Execute Query
        if ($stmt->execute()) {
            return true;
        }

        // Print Error
        printf('Error: %s.\n', $stmt->errorInfo());

        return false;
    }
}