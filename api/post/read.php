<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once  '../../config/Database.php';
include_once  '../../models/Post.php';

// Instantiate DB & Connect
$database = new Database();
$db = $database->connect();

// Instatiate Blog Post Object
$post = new Post($db);

// Blog Post Query
$result = $post->read();

// Get Row Count
$total = $result->rowCount();

// Check If Any Post
if($total <= 0) {
    echo json_encode(
        array('message' => 'No Posts Found')
    );
    return;
}

$postArr = array();

while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
    extract($row);
    $postItem = array(
        'id' => $id,
        'title' => $title,
        'body' => html_entity_decode($body),
        'author' => $author,
        'categoryId' => $category_id,
        'categoryName' => $category_name
    );

    // Push to "data"
    array_push($postArr, $postItem);
}

// Turn to JSON & Output
echo json_encode($postArr);
