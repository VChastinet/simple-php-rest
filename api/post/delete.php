<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: DELETE');
header(
    'Access-Control-Allow-Headers:
    Access-Control-Allow-Headers,
    Content-Type,
    Access-Control-Allow-Methods,
    Authorization,
    X-requested-with'
);

include_once  '../../config/Database.php';
include_once  '../../models/Post.php';

// Instantiate DB & Connect
$database = new Database();
$db = $database->connect();

// Instatiate Blog Post Object
$post = new Post($db);

// Get Raw Posted Data
$data = json_decode(file_get_contents('php://input'));

// Set ID To Delete
$post->id = $data->id;

// Delete Post

if (!$post->delete()) {
    echo json_encode(
        array('message' => 'Post Not Deleted')
    );
    return;
}

echo json_encode(
    array('message' => 'Post Deleted')
);
