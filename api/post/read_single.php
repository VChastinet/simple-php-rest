<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once  '../../config/Database.php';
include_once  '../../models/Post.php';

// Instantiate DB & Connect
$database = new Database();
$db = $database->connect();

// Instatiate Blog Post Object

$post = new Post($db);

// Get Id (from url)
$post->id = isset($_GET['id']) ? $_GET['id'] : die();
// Blog Post Query
$post->readSingle();

// Create array
$post_arr = array(
  'id' => (int)$post->id,
  'title' => $post->title,
  'body' => html_entity_decode($post->body),
  'author' => $post->author,
  'categoryId' => $post->categoryId ,
  'categoryName' => $post->categoryName,
);

// Make JSON
print_r(json_encode($post_arr));