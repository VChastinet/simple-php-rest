<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header(
    'Access-Control-Allow-Headers:
    Access-Control-Allow-Headers,
    Content-Type,
    Access-Control-Allow-Methods,
    Authorization,
    X-requested-with'
);

include_once  '../../config/Database.php';
include_once  '../../models/Post.php';

// Instantiate DB & Connect
$database = new Database();
$db = $database->connect();

// Instatiate Blog Post Object
$post = new Post($db);

// Get Raw Posted Data
$data = json_decode(file_get_contents('php://input'));

// Set ID To Update
$post->id = $data->id;

$post->title = $data->title;
$post->body = $data->body;
$post->author = $data->author;
$post->categoryId = $data->categoryId;

// Update Post

if (!$post->update()) {
    echo json_encode(
        array('message' => 'Post Not Updated')
    );
    return;
}

echo json_encode(
    array('message' => 'Post Updated')
);
