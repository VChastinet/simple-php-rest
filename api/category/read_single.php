<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once  '../../config/Database.php';
include_once  '../../models/Category.php';

// Instantiate DB & Connect
$database = new Database();
$db = $database->connect();

// Instatiate Blog Post Object

$category = new Category($db);

$category->id = isset($_GET['id']) ?  $_GET['id'] : die();

$category->readSingle();

$categoryArr = array(
    'id' => $category->id,
    'name' => $category->name,
    'createdAt' => $category->createdAt
);

print_r(json_encode($categoryArr));