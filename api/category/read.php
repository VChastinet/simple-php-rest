<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Cotent-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Category.php';

// Intantiiate DB & Connect
$database = new Database();
$db = $database->connect();

// Instatiante category Object
$category = new Category($db);

// Blog Category Query
$result = $category->read();

// Get Row Count
$total = $result->rowCount();

// Check If Any Category
if($total <= 0) {
    echo json_encode(
        array('message' => 'No Posts Found')
    );
    return;
}

$categoryArr = array();

while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
    extract($row);
    $categoryItem = array(
        'id' => $id,
        'name' => $name,
        'createdAt' => $created_at
    );

    // Push To Data
    array_push($categoryArr, $categoryItem);
}

// Turn To JSON Output
echo json_encode($categoryArr);