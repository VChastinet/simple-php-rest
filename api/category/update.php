<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header(
    'Access-Control-Allow-Headers:
    Access-Control-Allow-Headers,
    Content-Type,
    Access-Control-Allow-Methods,
    Authorization,
    X-requested-with'
);

include_once  '../../config/Database.php';
include_once  '../../models/Category.php';

// Instantiate DB & Connect
$database = new Database();
$db = $database->connect();

// Instatiate Blog Post Object
$category = new Category($db);

// Get Raw Posted Data
$data = json_decode(file_get_contents('php://input'));
// Set ID To Update
$category->id = $data->id;

$category->name = $data->name;
// Update category

if (!$category->update()) {
    echo json_encode(
        array('message' => 'Post Not Updated')
    );
    return;
}

echo json_encode(
    array('message' => 'Post Updated')
);
