<?php
    class Database {
        // DB Params

        /**
         * @var String
         */
        private $host = 'localhost';
        /**
         * @var String
         */
        private $dbName = 'maindb';
        /**
         * @var String
         */
        private $username = 'vchastinet';
        /**
         * @var String
         */
        private $password = 'root';

        /**
         * @var PDO
         */
        private $connection;

        //DB Connect
        public function connect() {
            $this->connection = null;
            $dsn = "pgsql:host=$this->host;dbname=$this->dbName;user=$this->username;password=$this->password";


            try {
                $this->connection = new PDO($dsn);
                $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $exception) {
                echo 'Connection Error: ' . $exception->getMessage();
            }

            return $this->connection;
        }
    }
